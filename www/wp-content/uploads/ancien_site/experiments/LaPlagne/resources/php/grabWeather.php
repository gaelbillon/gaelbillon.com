<?php
header('Content-Type: text/html; charset=utf-8');
# create and load the HTML
include('simple_html_dom.php');
$html = file_get_html('http://www.la-plagne.com/fr/hiver/autres/bulletin-neige.html');

foreach($html->find('div[class=bulletin_encart]') as $article) {
    $item['h3']     = $article->find('h3', 0)->plaintext;
    $item['p']    = $article->find('p', 0)->plaintext;
    $articles[] = $item;
}
$myArray['root'] = $articles;
echo json_encode($myArray);
?>