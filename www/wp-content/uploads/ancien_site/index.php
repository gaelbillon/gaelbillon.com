<?php
include_once "header.html";
?>
  <body>

    <!-- NAVBAR
    ================================================== -->
    <div class="navbar-wrapper">
      <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
      <div class="container">

        <div class="navbar navbar-inverse">
          <div class="navbar-inner">
            <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="brand" href="http://gaelbillon.com">GaelBillon.com</a>
            <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
            <div class="nav-collapse collapse">
              <ul class="nav">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="resume.php">Resume</a></li>
                <!-- <li><a href="experiments.php">Experiments</a></li> -->
                <li><a href="contact.php">Contact</a></li>
                <!-- Read about Bootstrap dropdowns at http://twitter.github.com/bootstrap/javascript.html#dropdowns -->
              </ul>
            </div><!--/.nav-collapse -->
          </div><!-- /.navbar-inner -->
        </div><!-- /.navbar -->

      </div> <!-- /.container -->
    </div><!-- /.navbar-wrapper -->



    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide">
      <div class="carousel-inner">
        <div class="item active">
          <img src="assets/img/slides/parthenay.jpg" alt="slide parthenay">
          <div class="container">
            <div class="carousel-caption">
              <h1>Parthenay</h1>
              <p class="lead">Web App for the community of communes of Parthenay, made with JQuery Mobile, Require.js, handlebars.js, QUnit, energize.js, modernizr, PhotoSwipe, iScroll CoffeScript, BitBucket, custom build script, TimThumb., google maps & Facebook API. Customisation of JQuery Mobile, see it on <a href="https://github.com/gaelbillon/jquery-mobile---no-headers-in-nested-sublists">GitHub</a></p>
              <a class="btn btn-large btn-primary" href="http://gaelbillon.fr/parthenay/pages/" target="_blank">Parthenay mobile site</a>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="assets/img/slides/haze_festival.jpg" alt="slide haze festival application">
          <div class="container">
            <div class="carousel-caption">
              <h1>Haze festival</h1>
              <p class="lead">App for a Danish music festival, sencha touch 2.0, sencha sdk tools, google closure compiler for obfuscation and minification PhoneGap for native iOS, Android &amp; BlackBerry apps, BitBucket for code versionning, wiki and issue tracker, the apps were available for the time of the festival on google play and the AppStore. The app can also run as a web application.</p>
              <a class="btn btn-large btn-primary" href="http://gaelbillon.fr/hazefestival" target="_blank">
              Haze Festival mobile web app</a>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="assets/img/slides/nature_capitale.jpg" alt="slide mobile site nature capitale">
          <div class="container">
            <div class="carousel-caption">
              <h1>Nature Capitale</h1>
              <p class="lead">Mobile site/Audio-guide (javascrit, css &amp; html5) for the event <a href="http://www.naturecapitale.com/">"nature capitale"</a> on the 23 et 24 may 2010 where more than 2 millions visitors came to take a walk on the Champs-Élysées with a garden makeover.</p>
              <a class="btn btn-large btn-primary" href="http://gaelbillon.fr/nature_capitale/mobile/index.php" target="_blank">Nature Capitale mobile site</a>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="assets/img/slides/asloservices.jpg" alt="slide asloservice site">
          <div class="container">
            <div class="carousel-caption">
              <h1>Le Chalet D'Aslo</h1>
              <p class="lead">The "Chalet D'Aslo" website, wordpess + custom theme, responsive design, speed optmization, natural SEO, adwords &nbsp; facebook API.</p>
              <a class="btn btn-large btn-primary" href="http://lechaletdaslo.com" target="_blank">LeChaletDAslo.com</a>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="assets/img/slides/myorpheo.jpg" alt="slide myorpheo website">
          <div class="container">
            <div class="carousel-caption">
              <h1>MyOrpheo</h1>
              <p class="lead">Website for the n°1 Audioguide company + several mobile web aps running on Blackberry, Android &amp; iOS.PHP, Photoshop, Analytics.</p>
              <a class="btn btn-large btn-primary" href="http://myorpheo.com" target="_blank">Myorpheo.com</a>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="assets/img/slides/auto-ecole.jpg" alt="slide auto-ecole website">
          <div class="container">
            <div class="carousel-caption">
              <h1>Auto école</h1>
              <p class="lead">Driving school website with responsive design &amp; mobile-first approach. HTML5, CSS3, Javascript, Photoshop &amp; responsive design.</p>
              <a class="btn btn-large btn-primary" href="http://autoecolepontoise.com" target="_blank">AutoEcolePontoise.com</a>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div><!-- /.carousel -->


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

            <!-- START THE FEATURETTES -->

            <div class="featurette">
              <img class="featurette-image pull-right" src="assets/img/features_thumbnails/idpicture.jpg" alt="photo identité">
              <h2 class="featurette-heading">About <span class="muted">me.</span></h2>
              <p class="lead">Genuinely interested and self motivated with a keen eye for detail, always going the extra mile to deliver a perfect product on time. Always curious about the latest cutting-edge web technologies.</p>
              <p class="lead">Highly competent with responsive design, cross-platform applications, speed optimization, offline web apps, geolocation, object oriented Javascript, MVC architecture.</p>
            </div>

            <hr class="featurette-divider">

            <!-- /END THE FEATURETTES -->

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="span4">
          <img class="img-circle" src="assets/img/features_thumbnails/javascript.png" alt="javascript logo">
          <h2>Javascript</h2>
          <p>Backbone, Angular, Nodejs, Sencha Touch, Require, Object Oriented Javascript</p>
          <!-- <p><a class="btn" href="#">View details &raquo;</a></p> -->
        </div><!-- /.span4 -->
        <div class="span4">
          <img class="img-circle" src="assets/img/features_thumbnails/html5.png" alt="html5 logo">
          <h2>HTML5</h2>
          <p>Audio, video, localStorage, indexedDB, offline, WebGL</p>
        </div><!-- /.span4 -->
        <div class="span4">
          <img class="img-circle" src="assets/img/features_thumbnails/tdd.png" alt="tdd logo">
          <h2>Test driven development</h2>
          <p></p>
        </div><!-- /.span4 -->
      </div><!-- /.row -->

      <!-- Second features row -->
      <div class="row">
        <div class="span4">
          <img class="img-circle" src="assets/img/features_thumbnails/jenkins.png" alt="jenkins logo">
          <h2>Continuous integration</h2>
          <p></p>
        </div><!-- /.span4 -->
        <div class="span4">
          <img class="img-circle" src="assets/img/features_thumbnails/phonegap.png" alt="phonegap logo">
          <h2>PhoneGap</h2>
          <!-- <p></p> -->
        </div><!-- /.span4 -->
        <div class="span4">
          <img class="img-circle" src="assets/img/features_thumbnails/git.png" alt="git logo">
          <h2>Git</h2>
          <p></p>
        </div><!-- /.span4 -->
      </div><!-- /.row -->

      <hr class="featurette-divider">

      <!-- End second features row -->

      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2013 Gaël Billon</p>
      </footer>

    </div><!-- /.container -->




<?php
include_once "footer.html";
?>