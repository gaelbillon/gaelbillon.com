<?php

return array(
	'version' => '0.9.4.1',
	'browsercache.configuration_sealed' => false,
	'cdn.configuration_sealed' => false,
	'common.install' => 1368377508,
	'common.visible_by_master_only' => true,
	'dbcache.configuration_sealed' => false,
	'minify.configuration_sealed' => false,
	'objectcache.configuration_sealed' => false,
	'pgcache.configuration_sealed' => false,
	'previewmode.enabled' => false,
	'varnish.configuration_sealed' => false,
	'fragmentcache.configuration_sealed' => false,
	'newrelic.configuration_sealed' => false,
	'extensions.configuration_sealed' => array(
	),
	'notes.minify_error' => true,
	'minify.error.last' => 'File "/home/sites/gaelbillon.com/www/wp-content/themes/enfold/js/prettyPhoto/js/jquery.prettyPhoto.js" doesn\'t exist',
	'minify.error.notification' => 'admin',
	'minify.error.notification.last' => 0,
	'minify.error.file' => 'hY9RDsIwCIYvNKxmJl7DK5AOty5tqZQZe3tbnckWH3wggZ-PH-jNfF9IStevCQQ3CiodgovdyVjBwhFyiYpPmNw4-RpKYuZsKrICb_poKN7YD62FD4dgOSTU7mxqEigqCCVfvs5cLa3CjSXAxbho_TJQbsOfSw6t8x_OVlzS_LN9J-SJRS3XkZ2chFTLdWLlzd6NuqMDDQ7JU3tlVwDGAZLHQtLufQE.js',
	'track.maxcdn_signup' => 0,
	'track.maxcdn_authorize' => 0,
	'track.maxcdn_validation' => 0,
	'notes.maxcdn_whitelist_ip' => true,
	'notes.remove_w3tc' => false,
	'notes.hide_extensions' => array(
	),
	'evaluation.reminder' => 1409039099,
);